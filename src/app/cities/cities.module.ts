import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CitiesContainerComponent } from './cities-container/cities-container.component';
import { CityListComponent } from './cities-container/city-list/city-list.component';
import { AddCityComponent } from './cities-container/add-city/add-city.component';
import { PageContentComponent } from '../weather-info/page-content/page-content.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [CityListComponent, AddCityComponent, CitiesContainerComponent],
  imports: [
    CommonModule, MDBBootstrapModule
  ],
  exports: [
    CityListComponent, AddCityComponent, CitiesContainerComponent
  ]
})
export class CitiesModule { }
