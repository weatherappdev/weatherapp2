import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { CitiesModule } from './cities/cities.module';
import { UserLoginModule } from './user-login/user-login.module';
import { WeatherInfoModule } from './weather-info/weather-info.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { PageContentComponent } from './weather-info/page-content/page-content.component';

@NgModule({
  declarations: [
    AppComponent, PageContentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,CoreModule, CitiesModule, UserLoginModule, WeatherInfoModule, MDBBootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
