import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginFormComponent } from './login-form/login-form.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [LoginFormComponent],
  imports: [
    CommonModule, MDBBootstrapModule, FormsModule, ReactiveFormsModule
  ],
  exports: [
    LoginFormComponent
  ]
})
export class UserLoginModule { }
