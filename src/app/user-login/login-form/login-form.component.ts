import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserlocationService } from 'src/app/userlocation.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})

export class LoginFormComponent implements OnInit {
    loginForm : FormGroup;
   storedEmail = [];
  constructor( private fb: FormBuilder, private data: UserlocationService) { }

  onLogin(){
    let userEmail: any = this.loginForm.controls.email.value;
    this.storedEmail.push(userEmail);
    localStorage.setItem('userEmail', JSON.stringify(this.storedEmail));
    console.log(this.storedEmail);
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['',Validators.compose([Validators.required, Validators.email])]
    })
    this.data.getLocation();
    this.data.theLocation.subscribe( res => { console.log(res)});
  }

}
