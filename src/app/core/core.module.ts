import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { DateComponent } from './date/date.component';
import { SearchCityComponent } from './search-city/search-city.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';


@NgModule({
  declarations: [NavbarComponent, DateComponent, SearchCityComponent],
  imports: [
    CommonModule,  MDBBootstrapModule
  ],
  exports: [
    NavbarComponent, DateComponent, SearchCityComponent
  ]
})
export class CoreModule { }
