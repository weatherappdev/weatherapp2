export class WeatherInfoModel {
  name: string;
  id: number;
  cod: string;

  constructor(name: string, id: number, cod: string) {
      this.name = name;
      this.id = id;
      this.cod = cod;
  }
}
