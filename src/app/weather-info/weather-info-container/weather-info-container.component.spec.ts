import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherInfoContainerComponent } from './weather-info-container.component';

describe('WeatherInfoContainerComponent', () => {
  let component: WeatherInfoContainerComponent;
  let fixture: ComponentFixture<WeatherInfoContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherInfoContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherInfoContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
