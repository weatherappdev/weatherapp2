import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weather-info-container',
  templateUrl: './weather-info-container.component.html',
  styleUrls: ['./weather-info-container.component.scss']
})
export class WeatherInfoContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
