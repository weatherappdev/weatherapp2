import { Component, OnInit } from '@angular/core';
import { UserlocationService } from 'src/app/userlocation.service';

@Component({
  selector: 'app-page-content',
  templateUrl: './page-content.component.html',
  styleUrls: ['./page-content.component.scss']
})
export class PageContentComponent implements OnInit {

  constructor( private data: UserlocationService) { }

  ngOnInit() {
    this.data.getLocation();
  }

}
