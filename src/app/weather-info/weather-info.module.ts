import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherInfoContainerComponent } from './weather-info-container/weather-info-container.component';
import { WeatherIconComponent } from './weather-info-container/weather-icon/weather-icon.component';
import { WeatherStatusComponent } from './weather-info-container/weather-status/weather-status.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { PageContentComponent } from './page-content/page-content.component';

@NgModule({
  declarations: [WeatherIconComponent, WeatherStatusComponent, WeatherInfoContainerComponent],
  imports: [
    CommonModule, MDBBootstrapModule
  ],
  exports: [
    WeatherIconComponent, WeatherStatusComponent, WeatherInfoContainerComponent
  ]
})
export class WeatherInfoModule { }
