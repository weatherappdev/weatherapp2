import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { WeatherInfoModel } from './weather-info-model';

@Injectable({
  providedIn: 'root'
})
export class WeatherInfoService {
  WeatherInfo: WeatherInfoModel[] = [];
  PATH = 'http://api.openweathermap.org/data/2.5/weather?lat=35&lon=139';
 // Observable stream for WeatherInfo Array
 weather: Subject<WeatherInfoModel[]> = new BehaviorSubject<WeatherInfoModel[]>(null);
  constructor(private http: HttpClient) { }

  getWeatherInfo(): Observable<any> {
    return this.http.get(this.PATH);
  }

  getWeatherInfoData() {
    this.getWeatherInfo().subscribe(
      res => {
        console.log(res.results);
        this.extractData(res.results);
      },
      (err: HttpErrorResponse) => {
        console.log(err);
      }
    );
  }

  extractData(results) {
    // let s: WeatherInfoModel[] = [];
    const s: WeatherInfoModel[] = [];
    for (const weather of results) {
    s.push(
      new WeatherInfoModel(weather.name, weather.id, weather.cod)
      );
     console.log(s);
    }
    this.weather.next(s);
  }



}
