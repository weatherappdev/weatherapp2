import { Injectable } from '@angular/core';
import { userLocation } from './location.model';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserlocationService {
   location: userLocation[];
   theLocation: Subject<userLocation[]> = new BehaviorSubject<userLocation[]>(null);
  constructor() {  }

  getLocation() {
    let loc: userLocation[] = [];
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition( position => {
        loc.push(
          new userLocation(position.coords.latitude,position.coords.longitude)
        ) 
        console.log(position.coords.latitude);
        console.log(position.coords.longitude);
       })          
    }
    this.theLocation.next(loc);

}
}